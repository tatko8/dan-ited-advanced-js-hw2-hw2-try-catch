const books = [
  { 
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70 
  }, 
  {
   author: "Сюзанна Кларк",
   name: "Джонатан Стрейндж і м-р Норрелл",
  }, 
  { 
    name: "Дизайн. Книга для недизайнерів.",
    price: 70
  }, 
  { 
    author: "Алан Мур",
    name: "Неономікон",
    price: 70
  }, 
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40
  },
  {
   author: "Анґус Гайленд",
   name: "Коти в мистецтві",
  }
];

console.log(books);

let div = document.createElement("div");
let ul = document.createElement("ul");
div.setAttribute("id", "root");
document.body.appendChild(div);
div.appendChild(ul);

const findProp = ["author", "name", "price"];
books.forEach((book) => {
  try {
    if (findProp.every((p) => book.hasOwnProperty(p))) {
      let li = document.createElement("li");
      li.innerHTML = `${book.author}, ${book.name}, ${book.price}`;
      ul.appendChild(li);
    } else {
      const unavailableProp = findProp.find((p) => !book.hasOwnProperty(p));
      throw new Error(`У елементі немає властивості ${unavailableProp}`);
    }
  } catch (error) {
    console.log(error.message);
  }
});
